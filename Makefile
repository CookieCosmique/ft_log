# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: bperreon <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2014/11/04 10:55:37 by bperreon          #+#    #+#              #
#    Updated: 2016/02/14 12:53:18 by bperreon         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = ft_log.a
CC = gcc
CFLAGS = -Wall -Wextra -Werror -O3
SOURCE = ./src/ft_log.c ./src/ft_logp.c \
		 ./src/ft_log_config.c ./src/ft_log_print.c
HEADER = -I./include/
OBJ = $(SOURCE:.c=.o)

all: $(NAME)

$(NAME): $(OBJ)
	ar rc $(NAME) $(OBJ)
	ranlib $(NAME)

%.o: %.c include
	$(CC) $(CFLAGS) $(HEADER) -c $< -o $@

clean:
	rm -rf $(OBJ)

fclean: clean
	rm -rf $(NAME)

re: fclean all
