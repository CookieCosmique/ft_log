/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_log_config.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bperreon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/14 12:32:37 by bperreon          #+#    #+#             */
/*   Updated: 2016/02/14 13:31:30 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_log.h"

t_log_config		*get_log_config(void)
{
	static t_log_config		*config = NULL;

	if (!config)
	{
		if (!(config = (t_log_config*)malloc(sizeof(t_log_config))))
			return (NULL);
		config->nb_tab = 0;
	}
	return (config);
}

void				ft_log_set_flags(const int flags)
{
	t_log_config	*config;

	if (!(config = get_log_config()))
		return ;
	config->flags = flags;
}

void				ft_log_add_flags(const int flags)
{
	t_log_config	*config;

	if (!(config = get_log_config()))
		return ;
	config->flags |= flags;
}

void				ft_log_tab(const int tab)
{
	t_log_config	*config;

	if (!(config = get_log_config()))
		return ;
	config->nb_tab += tab;
	if (config->nb_tab < 0)
		config->nb_tab = 0;
	if (config->nb_tab > 4)
		config->nb_tab = 4;
}
