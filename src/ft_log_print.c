/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_log_print.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bperreon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/14 12:49:27 by bperreon          #+#    #+#             */
/*   Updated: 2016/02/14 13:03:31 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_log.h"

void		ft_log_print(const int logtype, const char *s)
{
	static t_log_format		format[5] = {
		{"ERROR", C_BRED},
		{"FINE", C_GREEN},
		{"INFO", ""},
		{"WARN", C_YELLOW},
		{"DEBUG", C_CYAN}
	};
	t_log_config		*config;

	if (!(config = get_log_config()))
	{
		printf("%s[%s]%s\t%s\n", format[logtype].color, format[logtype].str,
				C_RESET, s);
		return ;
	}
	printf("%s[%s]%s\t%.*s%s\n", format[logtype].color, format[logtype].str,
			C_RESET, config->nb_tab * 2, "-\t-\t-\t-\t", s);
}
