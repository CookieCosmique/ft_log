/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_logp.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bperreon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/03 14:47:46 by bperreon          #+#    #+#             */
/*   Updated: 2016/02/14 13:07:12 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_log.h"

void		*ft_logp(const int logtype, const int tab, void *ptr,
		const char *str, ...)
{
	char					buffer[1024];
	va_list					arg;

	if (logtype == LOG_DEBUG && !(get_log_config()->flags & F_LOG_DEBUG))
		return (ptr);
	va_start(arg, str);
	vsprintf(buffer, str, arg);
	va_end(arg);
	ft_log_print(logtype, buffer);
	ft_log_tab(tab);
	return (ptr);
}
