/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_log.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bperreon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/03 14:47:46 by bperreon          #+#    #+#             */
/*   Updated: 2016/02/14 13:30:12 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_log.h"

int			ft_log(const int logtype, const int tab, const char *str, ...)
{
	char					buffer[1024];
	va_list					arg;

	if (logtype == LOG_DEBUG && !(get_log_config()->flags & F_LOG_DEBUG))
		return (logtype);
	va_start(arg, str);
	vsprintf(buffer, str, arg);
	va_end(arg);
	ft_log_print(logtype, buffer);
	ft_log_tab(tab);
	return (logtype);
}
