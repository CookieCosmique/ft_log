/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_log.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bperreon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/03 14:47:54 by bperreon          #+#    #+#             */
/*   Updated: 2016/02/14 13:28:01 by bperreon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_LOG_H
# define FT_LOG_H

# ifdef __cplusplus
    extern "C"{
# endif 

# include <stdarg.h>
# include <stdio.h>
# include <stdlib.h>

# define C_CLEAR ("\033[H\033[2J")
# define C_RESET ("\033[0m")
# define C_BOLD ("\033[1m")
# define C_REV ("\033[7m")
# define C_RESET ("\033[0m")
# define C_WHITE ("\x1B[0m")
# define C_RED ("\x1B[31m")
# define C_GREEN ("\x1B[32m")
# define C_YELLOW ("\x1B[33m")
# define C_BLUE ("\x1B[34m")
# define C_MAGENTA ("\x1B[35m")
# define C_CYAN ("\x1B[36m")
# define C_GRAY ("\033[22;37m")

# define C_BWHITE ("\033[1m\x1B[0m")
# define C_BRED ("\033[1m\x1B[31m")
# define C_BGREEN ("\033[1m\x1B[32m")
# define C_BYELLOW ("\033[1m\x1B[33m")
# define C_BBLUE ("\033[1m\x1B[34m")
# define C_BMAGENTA ("\033[1m\x1B[35m")
# define C_BCYAN ("\033[1m\x1B[36m")
# define C_BGRAY ("\033[1m\033[22;37m")

enum			e_logtype
{
	LOG_ERROR,
	LOG_FINE,
	LOG_INFO,
	LOG_WARNING,
	LOG_DEBUG
};

typedef struct	s_log_format
{
	char	str[8];
	char	color[16];
}				t_log_format;

enum			e_log_flags
{
	F_LOG_DEBUG = 1
};

typedef struct	s_log_config
{
	int			nb_tab;
	int			flags;
}				t_log_config;

t_log_config	*get_log_config(void);

int				ft_log(const int logtype, const int tab, const char *str, ...);
void			*ft_logp(const int logtype, const int tab, void *ptr,
		const char *str, ...);

void			ft_log_print(const int logtype, const char *s);

void			ft_log_tab(const int tab);
void			ft_log_set_flags(const int flags);
void			ft_log_add_flags(const int flags);

# ifdef __cplusplus
    }
# endif 

#endif
